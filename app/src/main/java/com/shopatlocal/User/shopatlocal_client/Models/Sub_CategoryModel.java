package com.shopatlocal.User.shopatlocal_client.Models;

import java.io.Serializable;

/**
 * Created by hp on 8/9/2017.
 */

public class Sub_CategoryModel implements Serializable {
    private String title;
    private Integer image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}