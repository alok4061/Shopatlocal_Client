package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.Constants;
import com.shopatlocal.User.shopatlocal_client.Utilities.MyProgressDialog;
import com.shopatlocal.User.shopatlocal_client.Utilities.NetworkConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    EditText nametxt,emailtxt,conttxt,passtxt;
    Button signbtn;
    TextView loginbtn;
    int mStatusCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        init();
    }
    private void init(){
        nametxt =findViewById(R.id.regname);
        emailtxt =findViewById(R.id.regemail);
        conttxt =findViewById(R.id.regcont);
        passtxt =findViewById(R.id.regpass);
        signbtn =findViewById(R.id.regsignupbtn);
        loginbtn =findViewById(R.id.regloginbtn);
        signbtn.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.regsignupbtn:
                    signupapi(conttxt.getText().toString(),emailtxt.getText().toString(),nametxt.getText().toString(),passtxt.getText().toString());
                break;
            case R.id.regloginbtn:
                break;
        }
    }
    private void signupapi(String mob,String email,String username,String passw){
        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put("_csrf", token12);
            jsonObject.put("name",username );
            jsonObject.put("email",email);
            jsonObject.put("phone", mob);
            jsonObject.put("password", passw);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        MyProgressDialog.showDialog(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, NetworkConstant.signup,jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                MyProgressDialog.hideDialog();
                try {
                    if (mStatusCode==200){
                        Log.d("response",jsonObject.toString());
                        String name =jsonObject.getString("name");
                        String emialid =jsonObject.getString("email");
                        String phone =jsonObject.getString("phone");
                        String pass =jsonObject.getString("encryptedPassword");
                        String id =jsonObject.getString("id");
                        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
                        SharedPreferences.Editor editor =sharedPreferences.edit();
                        editor.putString("User_id",id);
                        editor.apply();
                        Intent intent = new Intent(RegistrationActivity.this,MapsActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();

                String responseBody = null;
                try {
                    responseBody = new String( volleyError.networkResponse.data, "utf-8" );
                    JSONObject jsonObject = new JSONObject( responseBody );
                    if (jsonObject.getString("status").equals("Unsuccess")){
                        Toast.makeText(RegistrationActivity.this,"Please Correct email id and other fields",Toast.LENGTH_SHORT).show();
                    }
                    Log.e("res123.......",jsonObject.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }){
            @Override
            protected com.android.volley.Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......",mStatusCode+"");
                return super.parseNetworkResponse(response);
            }
        };
        RequestQueue requestQueue =Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);

    }

}
