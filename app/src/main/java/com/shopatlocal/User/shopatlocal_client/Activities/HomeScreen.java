package com.shopatlocal.User.shopatlocal_client.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.shopatlocal.User.shopatlocal_client.R;

public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
    }
}
