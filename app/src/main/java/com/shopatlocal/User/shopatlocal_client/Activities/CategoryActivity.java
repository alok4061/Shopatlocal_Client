package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.shopatlocal.User.shopatlocal_client.Adaptors.CategoryAdaptor;
import com.shopatlocal.User.shopatlocal_client.Adaptors.CategoryCardAdaptor;
import com.shopatlocal.User.shopatlocal_client.Adaptors.CategoryClickListner;

import com.shopatlocal.User.shopatlocal_client.Adaptors.CategoryListAdaptor;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryModel;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryResultModel;
import com.shopatlocal.User.shopatlocal_client.Models.Sub_CategoryModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.MyProgressDialog;
import com.shopatlocal.User.shopatlocal_client.Utilities.NetworkConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {
    ArrayList imageArray = new ArrayList();
    ArrayList namearray = new ArrayList();
    ArrayList id_list = new ArrayList();
    Sub_CategoryModel sub_categoryModel = new Sub_CategoryModel();
    CategoryResultModel categoryResultModel = new CategoryResultModel();
    RecyclerView subcat_rec;
    CategoryAdaptor categoryAdaptor;
    String owner_id, id;
    CategoryModel categoryModel;
    JSONArray subs_array;
    ArrayList subcat = new ArrayList();
    ArrayList imagepic =new ArrayList();
    ArrayList<CategoryResultModel> myList;
    public static ArrayList<Object> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        subcat_rec = findViewById(R.id.cat_recycler);
        myList = (ArrayList<CategoryResultModel>) getIntent().getSerializableExtra("MyArray");
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<Object>) args.getSerializable("ARRAYLIST");
        Log.d("object",object.toString());
//        getCategory();
        RecyclerView.LayoutManager linearlayout =new LinearLayoutManager(this);
        subcat_rec.setLayoutManager(linearlayout);
        subcat_rec.addOnItemTouchListener(new CategoryClickListner(getApplicationContext(), new CategoryClickListner.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                Toast.makeText(CategoryActivity.this,"gkofkgvbi",Toast.LENGTH_SHORT).show();
//                if (subcat.size() > 0) {
                Intent intent = new Intent(CategoryActivity.this, ProfileDetailActivity.class);
                    intent.putExtra("BUNDLE", MapsActivity.id_list.get(position).toString());
                    startActivity(intent);
//                }
            }
        }));

        Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.demoimage);
//        ImageHelper.getRoundedCornerBitmap(icon,20);
        imagepic.add(R.drawable.demoimage);
        imagepic.add(R.drawable.demoimage);
        imagepic.add(R.drawable.demoimage);
        imagepic.add(R.drawable.demoimage);
        imagepic.add(R.drawable.demoimage);
        imagepic.add(R.drawable.demoimage);
        categoryAdaptor = new CategoryAdaptor(CategoryActivity.this, myList);
        Log.e("mydata", namearray.toString());
        subcat_rec.setAdapter(categoryAdaptor);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_grid) {
            CategoryListAdaptor categoryListAdaptor = new CategoryListAdaptor(CategoryActivity.this, myList);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
            subcat_rec.setLayoutManager(layoutManager);
            subcat_rec.setAdapter(categoryListAdaptor);
            return true;
        }

        if (id == R.id.action_list) {
            categoryAdaptor = new CategoryAdaptor(CategoryActivity.this, myList);
            Log.e("mydata", namearray.toString());
            subcat_rec.setAdapter(categoryAdaptor);
            RecyclerView.LayoutManager linearlayout =new LinearLayoutManager(this);
            subcat_rec.setLayoutManager(linearlayout);
            return true;
        }

        if (id == R.id.action_card) {
            CategoryCardAdaptor categoryCardAdaptor =new CategoryCardAdaptor(CategoryActivity.this,myList);
            Log.e("mydata", namearray.toString());
            subcat_rec.setAdapter(categoryCardAdaptor);
            RecyclerView.LayoutManager linearlayout =new LinearLayoutManager(this);
            subcat_rec.setLayoutManager(linearlayout);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getCategory() {
        MyProgressDialog.showDialog(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(NetworkConstant.category, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                MyProgressDialog.hideDialog();
                Log.d("category", jsonArray.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    categoryModel = new CategoryModel();
                    try {
                        JSONObject cat_object = jsonArray.getJSONObject(i);
                        categoryModel.setTitle(cat_object.getString("name"));
                        id = cat_object.getString("id");
                        subs_array = cat_object.getJSONArray("subs");
                        subcat.add(subs_array);
                        namearray.add(categoryModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                categoryAdaptor = new CategoryAdaptor(CategoryActivity.this, namearray);
                subcat_rec.setAdapter(categoryAdaptor);
            }
        },
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
