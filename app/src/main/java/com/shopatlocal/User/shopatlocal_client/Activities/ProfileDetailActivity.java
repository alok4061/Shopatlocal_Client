package com.shopatlocal.User.shopatlocal_client.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.shopatlocal.User.shopatlocal_client.Models.ImageModel;
import com.shopatlocal.User.shopatlocal_client.Models.OffersDetailModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.Constants;
import com.shopatlocal.User.shopatlocal_client.Utilities.NetworkConstant;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static com.shopatlocal.User.shopatlocal_client.Utilities.AskPermissions.PHONE_PERMISSION_CODE;
import static com.shopatlocal.User.shopatlocal_client.Utilities.AskPermissions.askphonePermission;

public class ProfileDetailActivity extends AppCompatActivity implements View.OnClickListener {
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    CirclePageIndicator indicatorCP;
    ArrayList imageModelArrayList = new ArrayList();
    ImageModel model;
    TextView viewall, nameofofftxt, distanctxt, addtxt, contactpertxt, numtxt, offtitaltxt, acutxt, distxt, validtxt, desctxt, addidestxt, addihourtxt, addimodeofpaytxt, addiinfotxt,noofferstxt;
    ImageView callIV,naviIV;
    SharedPreferences client_idPreferences;
    ArrayList<OffersDetailModel> offerslist = new ArrayList<>();
    OffersDetailModel offersDetailModel;
    String phoneno,client_id;
    LinearLayout offer_cont;
    CardView mycard;
    ArrayList imagelist = new ArrayList();

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail);
        Intent intent = getIntent();
        client_id = intent.getStringExtra("BUNDLE");
        init();
        getcompleteData();

    }
    private void init() {
        nameofofftxt = findViewById(R.id.nameofofficetxt);
        distanctxt = findViewById(R.id.pro_dis_txt);
        addtxt = findViewById(R.id.officeaddtxt);
        contactpertxt = findViewById(R.id.contact_name);
        numtxt = findViewById(R.id.contact_num);
        offtitaltxt = findViewById(R.id.offertitlevalue);
        acutxt = findViewById(R.id.actu_offer_rate);
        distxt = findViewById(R.id.dis_offer_rate);
        validtxt = findViewById(R.id.offertillvalue);
        desctxt = findViewById(R.id.offerdescriptionvalue);
        callIV = findViewById(R.id.call_num);
        viewPager = findViewById(R.id.detailsViewpagerVP);
        indicatorCP = findViewById(R.id.indicator);
        viewall = findViewById(R.id.view_all);
        addidestxt = findViewById(R.id.desc_txt);
        addihourtxt = findViewById(R.id.hour_txt);
        addimodeofpaytxt = findViewById(R.id.mode_txt);
        addiinfotxt = findViewById(R.id.additional_txt);
        naviIV = findViewById(R.id.navi_btn);
        offer_cont = findViewById(R.id.offer_container);
        noofferstxt = findViewById(R.id.noofferstxt);
        mycard= findViewById(R.id.offers_card);
//        model = new ImageModel();
//        model.setPic(R.drawable.demoimage);
//        imageModelArrayList.add(model);

        viewall.setOnClickListener(this);
        callIV.setOnClickListener(this);
        naviIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_all:
                Intent intent = new Intent(this, OfferListActivity.class);
                intent.putExtra("offerlist", offerslist);
                startActivity(intent);
                break;
            case R.id.call_num:
                askphonePermission(this);
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phoneno));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
                break;
            case R.id.navi_btn:
                Intent intent1 = new Intent(this,NavigationActivity.class);
                startActivity(intent1);
                break;

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == PHONE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phoneno));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission. Unable to access call", Toast.LENGTH_LONG).show();
            }
        }
    }
    public class ViewPagerAdapter extends PagerAdapter {
        Context mcontext;
        ArrayList imageModelArrayList;
        LayoutInflater layoutInflater;
        public ViewPagerAdapter(Context context, ArrayList<ImageModel> imageModelArrayList) {
            this.imageModelArrayList = imageModelArrayList;
            this.mcontext = context;
        }
        @Override
        public int getCount() {
            return imagelist.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) mcontext.getSystemService(mcontext.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.detail_images, container, false);
//            ImageView imageView;
            imageView = (ImageView) view.findViewById(R.id.detaiImageViewIV);

            Picasso.with(mcontext).load(NetworkConstant.get_images+imagelist.get(position)).into(imageView);
            ((ViewPager) container).addView(view);

//            imageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(ProfileDetailActivity.this, "" + position, Toast.LENGTH_SHORT).show();
//                    Intent imgIntent = new Intent(ProfileDetailActivity.this, PinchZoomActivity.class);
//                    imgIntent.putExtra("posotion",position);
//                    Bundle args = new Bundle();
//                    args.putSerializable("ARRAYLIST",(Serializable)imagelist);
//                    imgIntent.putExtra("BUNDLE",args);
//                    imgIntent.putExtra("imgArray",imagelist.get(position).toString());
//                    startActivity(imgIntent);
//                }
//            });
//        imageView.setImageResource(Integer.parseInt(imageModelArrayList.get(position).getPic()));
            return view;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
    private void getcompleteData(){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, NetworkConstant.complete_data + "?id=" + client_id, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("Response: ",jsonObject.toString());
                try {
                    String nameofowner = jsonObject.getString("nameOfOffice");
                    phoneno = jsonObject.getString("clientPhone");
                    String contactper =jsonObject.getString("contactPerson");


                        if (jsonObject.has("photos")) {
                            JSONArray imageArray = jsonObject.getJSONArray("photos");
                            if (imageArray.length()==0){

                            }
                            else {
                                for (int p = 0; p < imageArray.length(); p++) {
                                    File f = new File(imageArray.get(p).toString());
//                            model = new ImageModel();
//                        model.setPic();
                                    imagelist.add(f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1));
                                }
                            }
                    }
                    adapter = new ViewPagerAdapter(ProfileDetailActivity.this, imageModelArrayList);
                    viewPager.setAdapter(adapter);
                    indicatorCP.setViewPager(viewPager);
                    Log.d("Myarr",imagelist.toString());
                    JSONArray locArray =jsonObject.getJSONArray("location");
                    String address=null;
                    for (int j=0;j<locArray.length();j++){
                        JSONObject jsonObject1 = locArray.getJSONObject(j);
                            address = jsonObject1.getString("address");

                    }

                    JSONArray offerArray = jsonObject.getJSONArray("clientOffer");
                    if (offerArray.length()==0){
                        offer_cont.setVisibility(View.INVISIBLE);
                        mycard.setVisibility(View.INVISIBLE);
                        noofferstxt.setText("Offers are coming soon");
                    }

                    for (int i=0;i<offerArray.length();i++){
                        offer_cont.setVisibility(View.VISIBLE);
                        mycard.setVisibility(View.VISIBLE);
                        noofferstxt.setText("");
                        JSONObject jsonObject2 =offerArray.getJSONObject(i);
                        String tital = jsonObject2.getString("title");
                        String acuprice = jsonObject2.getString("actualPrice");
                        String discprice =jsonObject2.getString("discountPrice");
                        String validtill = jsonObject2.getString("validTill");
                        String description = jsonObject2.getString("description");
                        offersDetailModel= new OffersDetailModel();
                        offtitaltxt.setText(tital);
                        acutxt.setText(acuprice);
                        distxt.setText(discprice);
                        validtxt.setText(validtill);
                        desctxt.setText(description);
                        offersDetailModel.setTitle(tital);
                        offersDetailModel.setAcurateAmount(acuprice);
                        offersDetailModel.setDiscountAmount(discprice);
                        offersDetailModel.setValid(validtill);
                        offersDetailModel.setDescription(description);
                        offerslist.add(offersDetailModel);
                    }
//                    offersDetailModel= offerslist.get(0);
                    nameofofftxt.setText(nameofowner);
                    contactpertxt.setText(contactper);
                    numtxt.setText(phoneno);
                    addtxt.setText(address);

                    JSONArray client_infoArray =jsonObject.getJSONArray("clientInfo");
                    String des_txt="",hour_txt="",mode_txt="",additional_txt="";
                    for (int j=0;j<client_infoArray.length();j++){
                        JSONObject jsonObject3 = client_infoArray.getJSONObject(j);
                        des_txt = jsonObject3.getString("description");
                        hour_txt = jsonObject3.getString("hourOfOperation");
                        mode_txt = jsonObject3.getString("modeOfPayment");
                        additional_txt = jsonObject3.getString("additionalInfo");
                        addidestxt.setText(des_txt);
                        addihourtxt.setText(hour_txt);
                        addimodeofpaytxt.setText(mode_txt);
                        addiinfotxt.setText(additional_txt);
                        Log.d("ewejniejni",hour_txt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

}
