package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.shopatlocal.User.shopatlocal_client.Models.OffersDetailModel;
import com.shopatlocal.User.shopatlocal_client.R;
import java.util.ArrayList;

public class OfferListAdaptor extends ArrayAdapter{
    OffersDetailModel offersDetailModel=new OffersDetailModel();

    ArrayList<OffersDetailModel> tittle =new ArrayList();
    public OfferListAdaptor(@NonNull Context context, ArrayList<OffersDetailModel> tittle) {
        super(context, R.layout.activity_offer_list_adaptor,tittle);
        this.tittle =tittle;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_offer_list_adaptor,null);
        OffersDetailModel offersDetailModel = tittle.get(position);
        Log.d("firstvalue",offersDetailModel.getTitle());
        holder.titletxt = convertView.findViewById(R.id.offertitlevalue);
        holder.actiutxt = convertView.findViewById(R.id.actu_offer_rate);
        holder.descounttxtx = convertView.findViewById(R.id.dis_offer_rate);
        holder.valitxt = convertView.findViewById(R.id.offertillvalue);
        holder.distxt = convertView.findViewById(R.id.offerdescriptionvalue);
        holder.titletxt.setText(offersDetailModel.getTitle());
        holder.actiutxt.setText(offersDetailModel.getAcurateAmount());
        holder.descounttxtx.setText(offersDetailModel.getDiscountAmount());
        holder.valitxt.setText(offersDetailModel.getValid());
        holder.distxt.setText(offersDetailModel.getDescription());
        convertView.setTag(holder);
        return convertView;
    }
    static class ViewHolder {
        TextView titletxt,actiutxt,descounttxtx,valitxt,distxt;
    }
}
