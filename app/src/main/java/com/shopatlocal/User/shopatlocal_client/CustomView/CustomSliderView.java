package com.shopatlocal.User.shopatlocal_client.CustomView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.shopatlocal.User.shopatlocal_client.R;


import it.sephiroth.android.library.imagezoom.ImageViewTouch;

/**
 * Created by deepak on 25/11/16.
 */

public class CustomSliderView extends BaseSliderView {
    public CustomSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.fullscreen_image_display,null);
        ImageViewTouch target = (ImageViewTouch) v.findViewById(R.id.slider_image);
        bindEventAndShow(v, target);
        return v;
    }
}
