package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopatlocal.User.shopatlocal_client.Activities.CategoryActivity;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryResultModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.NetworkConstant;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CategoryAdaptor extends RecyclerView.Adapter<CategoryAdaptor.RecyclerHolder> {

    //    ArrayList<CategoryModel> namearraylist = new ArrayList();
    public static ArrayList<CategoryResultModel> namearraylists = new ArrayList();
    ArrayList<CategoryResultModel> distlist = new ArrayList<>();
    CategoryResultModel categoryResultModel;
    ArrayList imagelist = new ArrayList();
    Context mcontext;
    private static final String IMAGES_URL = "http://192.168.1.13:1339/images/";

    public CategoryAdaptor(Context context, ArrayList<CategoryResultModel> namearraylist) {
        this.mcontext = context;
        this.namearraylists = namearraylist;
        this.imagelist = imagelist;
    }

    @Override
    public CategoryAdaptor.RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_category_adaptor2, null);
        CategoryAdaptor.RecyclerHolder recyclerHolder = new CategoryAdaptor.RecyclerHolder(v);
        return recyclerHolder;
    }

    @Override
    public int getItemCount() {
        return namearraylists.size();
    }

    @Override
    public void onBindViewHolder(CategoryAdaptor.RecyclerHolder holder, int position) {
        CategoryResultModel categoryResultModel = namearraylists.get(position);
        Picasso.with(mcontext).load(NetworkConstant.get_images+ CategoryActivity.object.get(position)).into(holder.imageView);
        if (CategoryActivity.object.get(position).equals("null")){
            holder.imageView.setImageResource(R.drawable.ic_card_view);
        }
        holder.textView.setText(categoryResultModel.getTitle());
        double meter = Double.parseDouble(categoryResultModel.getDistance());
        double val = meter / 1000;
        DecimalFormat df2 = new DecimalFormat(".###");
        holder.disttxt.setText(df2.format(val) + " km");



    }

    public class RecyclerHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView, disttxt, addretxt;

        public RecyclerHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.cat_detail_img);
            textView = (TextView) itemView.findViewById(R.id.cat_detail_txt);
            addretxt = itemView.findViewById(R.id.cat_address_txt);
            disttxt = itemView.findViewById(R.id.dist_count);
        }
    }
}