package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryResultModel;
import com.shopatlocal.User.shopatlocal_client.R;

import java.util.ArrayList;

public class MapCategoryActivity extends AppCompatActivity implements OnMapReadyCallback{
    GoogleMap mMap;
    ArrayList<CategoryResultModel> myList;
    public static ArrayList<Object> object;
    int i=0;
    ArrayList countlist = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_category);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        myList = (ArrayList<CategoryResultModel>) getIntent().getSerializableExtra("MyArray");
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<Object>) args.getSerializable("ARRAYLIST");
        Log.d("object",object.toString());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        int cout =0;
        for (i = 0; i < myList.size(); i++) {
            double lati=Double.parseDouble(myList.get(i).getLat());
            double longLat=Double.parseDouble(myList.get(i).getLng());
           Marker marker = mMap.addMarker(new MarkerOptions().position(
                    new LatLng(lati,longLat))
                    .title(myList.get(i)
                            .getTitle())
//                    .snippet(pins.get(i).address)
            );
           marker.setTag(myList.get(i).getClient_id());
            Log.d("count",countlist.toString());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lati,longLat),15));
            // Zoom in, animating the camera.
            googleMap.animateCamera(CameraUpdateFactory.zoomIn());
            // Zoom out to zoom level 10, animating with a duration of 2 seconds.
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(MapCategoryActivity.this, ProfileDetailActivity.class);
                intent.putExtra("BUNDLE", marker.getTag().toString());
                startActivity(intent);
                }

        });
    }
}
