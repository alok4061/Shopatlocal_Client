package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopatlocal.User.shopatlocal_client.Models.Sub_CategoryModel;
import com.shopatlocal.User.shopatlocal_client.R;

import java.util.ArrayList;

public class SubCatListAdaptor extends ArrayAdapter {

    ArrayList sub_categoryModellist = new ArrayList();


    public SubCatListAdaptor(@NonNull Context context, ArrayList<Sub_CategoryModel> sub_categoryModellist) {
        super(context, R.layout.activity_sub_cat_list_adaptor, sub_categoryModellist);
        this.sub_categoryModellist = sub_categoryModellist;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_sub_cat_list_adaptor, null);

        ImageView sun_cat_img = view.findViewById(R.id.sub_cat_adp_img);
        TextView sub_cat_txt = view.findViewById(R.id.sub_cat_adp_txt);
        sub_cat_txt.setText(sub_categoryModellist.get(position).toString());
//        Log.e("size1234",sub_categoryModellist.get(position).getTitle());
//        sun_cat_img.setImageResource(imagearray.get(position).);
        return view;
    }
}
