package com.shopatlocal.User.shopatlocal_client.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.shopatlocal.User.shopatlocal_client.Adaptors.OfferListAdaptor;
import com.shopatlocal.User.shopatlocal_client.Models.OffersDetailModel;
import com.shopatlocal.User.shopatlocal_client.R;

import java.util.ArrayList;

public class OfferListActivity extends AppCompatActivity {
    ArrayList<OffersDetailModel> offersDetailModelArrayList;
    OffersDetailModel offersDetailModel =new OffersDetailModel();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_list);
        ListView listView = findViewById(R.id.offer_list);
        offersDetailModelArrayList = (ArrayList<OffersDetailModel>) getIntent().getSerializableExtra("offerlist");
        for (int i =0;i<offersDetailModelArrayList.size();i++){
            offersDetailModel = offersDetailModelArrayList.get(i);
            Log.d("ok",offersDetailModel.getTitle());
        }


        OfferListAdaptor offerListAdaptor = new OfferListAdaptor(this,offersDetailModelArrayList);
        listView.setAdapter(offerListAdaptor);


    }
}
