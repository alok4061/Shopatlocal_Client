package com.shopatlocal.User.shopatlocal_client.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shopatlocal.User.shopatlocal_client.Adaptors.CategoryAdaptor;
import com.shopatlocal.User.shopatlocal_client.Adaptors.GooglePlacesAutocompleteAdapter;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryModel;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryResultModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.AskPermissions;
import com.shopatlocal.User.shopatlocal_client.Utilities.Constants;
import com.shopatlocal.User.shopatlocal_client.Utilities.LacationUtils;
import com.shopatlocal.User.shopatlocal_client.Utilities.MyProgressDialog;
import com.shopatlocal.User.shopatlocal_client.Utilities.NetworkConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.shopatlocal.User.shopatlocal_client.Utilities.AskPermissions.LOCATION_PERMISSION_CODE;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, SeekBar.OnSeekBarChangeListener, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private GoogleMap mMap;
    ArrayList markerPoints = new ArrayList();
    ArrayList<CategoryResultModel> categorytypeList = new ArrayList<>();
    ArrayList<String> sub_categorytypeList = new ArrayList<>();
    ArrayList namearray = new ArrayList();
    CategoryAdaptor categoryAdaptor;
    String owner_id, id;
    CategoryModel categoryModel;
    JSONArray subs_array;
    ArrayList subcat = new ArrayList();
    ArrayList imagepic = new ArrayList();
    Spinner cateSpin, subCatSpin;
    SeekBar seekBar;
    Button editbtn, savebtn, quick_search,quick_map_search;
    FrameLayout currFram, CustomFram;
    AutoCompleteTextView autoAddress;
    List<Address> address;
    List<Address> address1;
    double longitude, latitude;
    TextView addressTV;
    String addresss, countryname, city, pincode, state, sector;
    LacationUtils utils;
    LatLng p1, center;
    Address location;
    int screenSize, seek_valu = 1000;
    TextView mycurloc;
    int mStatusCode;
    String sun_string;
    TextView label, seektxt;
    View row;
    public static ArrayList id_list = new ArrayList();
    ArrayList cat_image_list= new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        AskPermissions.askLocationStoragePermission(this);
        getScreenSize();
        init();
        getCategory();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        autoAddress = findViewById(R.id.myautocom);
        autoAddress.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.autolist_item));
        autoAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String str = (String) adapterView.getItemAtPosition(i);
                mycurloc.setText(str);
                View v =MapsActivity.this.getCurrentFocus();
                if (v == null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                final AutoCompleteTextView autoAddress = findViewById(R.id.myautocom);
                getLocationFromAddress(MapsActivity.this, str);
            }
        });
    }

    private void init() {
        cateSpin = findViewById(R.id.categspin);
        subCatSpin = findViewById(R.id.subCategspin);
        seekBar = findViewById(R.id.radiausSeekbar);
        CustomFram = findViewById(R.id.custom_loc_fram);
        mycurloc = findViewById(R.id.mycurrloc);
        quick_search = findViewById(R.id.quick_search);
        quick_map_search = findViewById(R.id.quick_map_search);
//        seektxt = findViewById(R.id.seektxt);
//        savebtn.setEnabled(false);
        autoAddress = findViewById(R.id.myautocom);
        seekBar.setOnSeekBarChangeListener(this);
        quick_search.setOnClickListener(this);
        quick_map_search.setOnClickListener(this);

//        CustomAdapter customAdapter = new CustomAdapter(this, R.layout.row2, categorytypeList);
//        cateSpin.setAdapter(customAdapter);
        CustomSubAdapter subcustomAdapter = new CustomSubAdapter(this, R.layout.row2, sub_categorytypeList);
        subCatSpin.setAdapter(subcustomAdapter);
        subCatSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        utils = new LacationUtils();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(utils.getlocation(this).latitude, utils.getlocation(this).longitude), 16));
        getAddress(utils.getlocation(this).latitude, utils.getlocation(this).longitude);

        mMap.setPadding(0, 600, 0, 0);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);
        center = mMap.getCameraPosition().target;
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.DONUT)
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                longitude = cameraPosition.target.longitude;
                latitude = cameraPosition.target.latitude;
                Log.e("longitude", longitude + "");
                Log.e("latitude", latitude + "");
                try {
                    address1 = new Geocoder(MapsActivity.this, Locale.getDefault()).getFromLocation(latitude, longitude, 1);
                    Log.e("longitude address", longitude + "");
                    Log.e("latitude address", latitude + "");
                    Log.e("Address ", address1 + "");
                    addresss = address1.get(0).getAddressLine(0);
                    city = address1.get(0).getLocality();
                    state = address1.get(0).getAdminArea();
                    countryname = address1.get(0).getCountryName();
                    pincode = address1.get(0).getPostalCode();
                    sector = address.get(0).getSubLocality();
                    mycurloc.setText(addresss + " " + sector + " " + city + " " + state + " " + countryname + " " + pincode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        i = i + 1;
//      seekBar.setThumb(writeOnDrawable(R.mipmap.ic_launcher_round, i + ""));
        seek_valu = i * 1000;
        Toast.makeText(MapsActivity.this, "Radius : " + i + " km", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.quick_search:
                quickSearch(true);
                break;
            case R.id.quick_map_search:
                quickSearch(false);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        sub_categorytypeList.clear();
        sun_string = subcat.get(i).toString();
        try {
            getjsonarray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public BitmapDrawable writeOnDrawable(int drawableId, String text) {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK); //Change this if you want other color of text
        paint.setTextSize(80);
        //Change this if you want bigger/smaller font
        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, bm.getWidth() / 4, bm.getHeight() / 1.3f, paint); //Change the position of the text here
        return new BitmapDrawable(bm);
    }

    public class CustomAdapter extends ArrayAdapter<String> {

        public CustomAdapter(Context context, int textViewResourceId,
                             ArrayList<String> stringArrayList) {
            super(context, textViewResourceId, stringArrayList);
            // TODO Auto-generated constructor stub
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            categoryModel = (CategoryModel) namearray.get(position);
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.row2, parent, false);
            TextView label = (TextView) row.findViewById(R.id.weekofday);

            label.setText(categoryModel.getTitle());
            cateSpin.setOnItemSelectedListener(MapsActivity.this);

          /*  ImageView icon=(ImageView)row.findViewById(R.id.icon);

            if (array[position]=="Sunday"){
                icon.setImageResource(R.drawable.icon);
            }
            else{
                icon.setImageResource(R.drawable.icongray);
            }*/

            return row;
        }

    }

    public class CustomSubAdapter extends ArrayAdapter<String> {

        public CustomSubAdapter(Context context, int textViewResourceId,
                                ArrayList<String> stringArrayList) {
            super(context, textViewResourceId, stringArrayList);
            // TODO Auto-generated constructor stub
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            row = inflater.inflate(R.layout.subcatrow, parent, false);
            label = (TextView) row.findViewById(R.id.sub_cat_row);
            label.setText(sub_categorytypeList.get(position));

          /*  ImageView icon=(ImageView)row.findViewById(R.id.icon);

            if (array[position]=="Sunday"){
                icon.setImageResource(R.drawable.icon);
            }
            else{
                icon.setImageResource(R.drawable.icongray);
            }*/

            return row;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.DONUT)
    private void getAddress(double latitude, double longitude) {
        Log.e("longitude", longitude + "");
        Log.e("latitude", latitude + "");
        try {
            address = new Geocoder(MapsActivity.this, Locale.getDefault()).getFromLocation(latitude, longitude, 1);
            Log.e("longitude address", longitude + "");
            Log.e("latitude address", latitude + "");
            Log.e("Address ", address + "");
            addresss = address.get(0).getAddressLine(0);
            city = address.get(0).getLocality();
            state = address.get(0).getAdminArea();
            countryname = address.get(0).getCountryName();
            pincode = address.get(0).getPostalCode();
            sector = address.get(0).getSubLocality();
            mycurloc.setText(addresss + " " + sector + " " + city + " " + state + " " + countryname + " " + pincode);
            Log.e("Address ", sector + "");
            p1 = new LatLng(latitude, longitude);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;


        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    p1, 15);
            mMap.addMarker(new MarkerOptions().position(p1));
            mMap.animateCamera(location);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    private void getScreenSize() {
        screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
    }


    //This method will be called when the user will tap on allow or deny
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == LOCATION_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission. unable to access the location", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void quickSearch(final boolean abc) {
        categorytypeList.clear();
        id_list.clear();
        MyProgressDialog.showDialog(this);
//        JsonArrayRequest stringRequest = new JsonArrayRequest(NetworkConstant.quick_Search+"?lat="+utils.getlocation(MapsActivity.this)
//                .latitude+"&lng="+utils.getlocation(MapsActivity.this).longitude+"&maxDistance="+seek_valu +"&category="
//                +categoryModel.getTitle()+"&subCategory="+subCatSpin.getSelectedItem(), new Response.Listener<JSONArray>() {
        Log.d("seekvalue,",seek_valu+"");
        JsonArrayRequest stringRequest = new JsonArrayRequest(NetworkConstant.quick_Search + "?lat=" + 28.6618976 + "&lng=" + 77.22739580000007 + "&maxDistance=" + seek_valu + "&category="
                + categoryModel.getTitle() + "&subCategory=" + URLEncoder.encode(subCatSpin.getSelectedItem().toString()), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
//                Log.d("url",NetworkConstant.quick_Search+"?lat="+utils.getlocation(MapsActivity.this).latitude+"&lng="+utils.getlocation(MapsActivity.this).longitude+"&maxDistance="+seek_valu +"&category="+categoryModel.getTitle()+"&subCategory="+subCatSpin.getSelectedItem());
                MyProgressDialog.hideDialog();
                Log.e("Response", jsonArray.toString());
                if (jsonArray.length() == 0) {
                    Toast.makeText(MapsActivity.this, "We will come back soon with good result. \n you can choose another option", Toast.LENGTH_LONG).show();
                } else {


                    JSONObject jsonObject = null;
//                try {
////                    jsonObject = new JSONObject(s);
////                    String status = jsonObject.getString("status");
////                    Toast.makeText(MapsActivity.this, "Mobile no and Password are not match" + status, Toast.LENGTH_LONG).show();
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String dis = jsonObject1.getString("dis");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("obj");
                            String nameofoff = jsonObject2.getString("nameOfOffice");
                            String client_id = jsonObject2.getString("owner");
                            id_list.add(client_id);
                            Log.d("my_shared_data", id_list.toString());
                            try {

                                if (jsonObject2.has("photos")) {
                                    JSONArray jsonArray1 = jsonObject2.getJSONArray("photos");

//                                    Log.d("imagesArray", jsonArray1.toString());
                                    File f = new File(jsonArray1.get(0).toString());
                                    cat_image_list.add(f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1).toString());
                                }

                                // pending should not reach to catch must be handle in else block

                                Log.d("myImage", cat_image_list.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                                cat_image_list.add("null");
                            }
                            JSONArray jsonArray1 = jsonObject2.getJSONArray("coordinates");
                            Log.d("Muvlaopp1",jsonArray1.get(0).toString());
                            Log.d("Muvlaopp2",jsonArray1.get(1).toString());
                       String mylng = jsonArray1.get(0).toString();
                       String mylat = jsonArray1.get(1).toString();
                            CategoryResultModel categoryResultModel = new CategoryResultModel();
                            categoryResultModel.setTitle(nameofoff);
                            categoryResultModel.setDistance(dis);
                            categoryResultModel.setLat(mylat);
                            categoryResultModel.setLng(mylng);
                            categoryResultModel.setClient_id(client_id);
                            categorytypeList.add(categoryResultModel);
                            Log.d("my values", categorytypeList.toString());
                        }
                        if (abc==true) {
                            Intent intent = new Intent(MapsActivity.this, CategoryActivity.class);
                            intent.putExtra("MyArray", categorytypeList);
                            Bundle args = new Bundle();
                            args.putSerializable("ARRAYLIST", (Serializable) cat_image_list);
                            intent.putExtra("BUNDLE", args);
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(MapsActivity.this, MapCategoryActivity.class);
                            intent.putExtra("MyArray", categorytypeList);
                            Bundle args = new Bundle();
                            args.putSerializable("ARRAYLIST", (Serializable) cat_image_list);
                            intent.putExtra("BUNDLE", args);
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();
                String responseBody = null;
                try {
                    responseBody = new String(volleyError.networkResponse.data, "utf-8");
//                    JSONObject jsonObject = new JSONObject(responseBody);
//                    if (jsonObject.getString("status").equals("Unsuccess")) {
//                        Toast.makeText(MapsActivity.this, "Mobile number and Password are not match", Toast.LENGTH_SHORT).show();
//                    }
//                    Log.e("res123.......", jsonObject.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getCategory() {
        MyProgressDialog.showDialog(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(NetworkConstant.category, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                MyProgressDialog.hideDialog();
                Log.d("category", jsonArray.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    categoryModel = new CategoryModel();
                    try {
                        JSONObject cat_object = jsonArray.getJSONObject(i);
                        categoryModel.setTitle(cat_object.getString("name"));
                        id = cat_object.getString("id");
                        subs_array = cat_object.getJSONArray("subs");
                        subcat.add(subs_array);
                        Log.d("sub_arrta", subs_array + "");
                        namearray.add(categoryModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                CustomAdapter customAdapter = new CustomAdapter(MapsActivity.this, R.layout.row2, namearray);
                cateSpin.setAdapter(customAdapter);
//                categoryAdaptor = new CategoryAdaptor(MapsActivity.this, namearray,imagepic);
                Log.e("mydata", namearray.toString());
//                subcat_rec.setAdapter(categoryAdaptor);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void getjsonarray() throws JSONException {
        JSONArray jsonArray = new JSONArray(sun_string);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");
//            subCategoryModel.setTitle(name);
            sub_categorytypeList.add(name);
            CustomSubAdapter subcustomAdapter = new CustomSubAdapter(this, R.layout.row2, sub_categorytypeList);
            subCatSpin.setAdapter(subcustomAdapter);
            Log.d("name", name);
        }
    }
}




