package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.shopatlocal.User.shopatlocal_client.Adaptors.SubCatListAdaptor;
import com.shopatlocal.User.shopatlocal_client.Models.Sub_CategoryModel;
import com.shopatlocal.User.shopatlocal_client.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubCategoryList extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ArrayList sub_categoryModellist = new ArrayList();

    Sub_CategoryModel subCategoryModel = new Sub_CategoryModel();
    String sun_string;
    SubCatListAdaptor subCatListAdaptor;
    ListView suncat_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_list);
        suncat_list = findViewById(R.id.sub_cat_list);
        Intent intent = getIntent();
        sun_string = intent.getStringExtra("BUNDLE");
        Log.e("size12345", sun_string);
        try {
            getjsonarray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        subCatListAdaptor = new SubCatListAdaptor(this, sub_categoryModellist);
        suncat_list.setAdapter(subCatListAdaptor);
        suncat_list.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, Sub_SubCategorylistActivity.class);
        startActivity(intent);

    }
    private void getjsonarray() throws JSONException {
        JSONArray jsonArray = new JSONArray(sun_string);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");
            subCategoryModel.setTitle(name);
            sub_categoryModellist.add(name);

            Log.d("name", name);
        }
    }
}

