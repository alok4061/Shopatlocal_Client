package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.AskPermissions;
import com.shopatlocal.User.shopatlocal_client.Utilities.Constants;

import static com.shopatlocal.User.shopatlocal_client.Utilities.AskPermissions.LOCATION_PERMISSION_CODE;

public class SplashScreen extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private LinearLayout linearLayout;
//    private List<TokenModal> tokens;
    RequestQueue requestQueue;
    String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        AskPermissions.askLocationStoragePermission(this);
        linearLayout = (LinearLayout) findViewById(R.id.ll);
        requestQueue = Volley.newRequestQueue(this);
//        getToken();
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putLong("user_id", 52);
//        editor.apply();

        new CountDownTimer(3000, 3000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                checkToProceed();
            }
        }.start();
    }

    private void checkToProceed() {

        if (sharedPreferences.contains("User_id")){
            goToMainActivity();
       }
        else{
            goToRegistrationActivity();
        }
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this,MapsActivity.class);
//        intent.putExtra("token",token);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
       finish();
    }

    private void goToRegistrationActivity() {
        Intent intent = new Intent(this, RegistrationActivity.class);
//        intent.putExtra("tokens",token);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == LOCATION_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission. Unable to access location", Toast.LENGTH_LONG).show();
            }
        }
    }
//    private void getToken(){
//        MyProgressDialog.showDialog(this);
//        Retrofit retrofit = new Retrofit.Builder().baseUrl(NetworkConstant.csrf_token+"/").addConverterFactory(GsonConverterFactory.create()).build();
//        GetTokenInterface getTokenInterface = retrofit.create(GetTokenInterface.class);
//
//        getTokenInterface.getTokens(new Callback<List<TokenModal>>() {
//            @Override
//            public void onResponse(Call<List<TokenModal>> call, Response<List<TokenModal>> response) {
//                MyProgressDialog.hideDialog();
//                tokens = (List<TokenModal>) call;
//                String name = response.toString();
//                Log.d("response",name);
//            }
//
//            @Override
//            public void onFailure(Call<List<TokenModal>> call, Throwable t) {
//
//            }
//        });
//
//    }
//    private void getToken(){
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, NetworkConstant.csrf_token, null, new com.android.volley.Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                try {
//                    token =jsonObject.getString("_csrf");
//                    Log.d("token",token);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//            }
//        });
//        requestQueue.add(jsonObjectRequest);
//    }
}
