package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by hp on 7/28/2017.
 */

public class Sub_SubCategoryClickListener implements RecyclerView.OnItemTouchListener{

    private CategoryClickListner.OnItemClickListener listener;
    GestureDetector mGestureDetector;

    public Sub_SubCategoryClickListener(Context applicationContext, CategoryClickListner.OnItemClickListener onItemClickListener) {
        listener=onItemClickListener;
        mGestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    public interface OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View view = rv.findChildViewUnder(e.getX(),e.getY());
        if(view!=null && listener!=null && mGestureDetector.onTouchEvent(e)){
            listener.onItemClick(view,rv.getChildAdapterPosition(view));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {


    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}

