package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopatlocal.User.shopatlocal_client.Activities.CategoryActivity;
import com.shopatlocal.User.shopatlocal_client.Activities.MapsActivity;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryResultModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by hp on 8/21/2017.
 */

public class CategoryListAdaptor extends RecyclerView.Adapter<CategoryListAdaptor.RecyclerHolder> {

    //    ArrayList<CategoryModel> namearraylist = new ArrayList();
    ArrayList<CategoryResultModel> namearraylists = new ArrayList();
    ArrayList imagelist = new ArrayList();
    private static final String IMAGES_URL = "http://192.168.1.13:1339/images/";
    Context mcontext;

    public CategoryListAdaptor(Context context, ArrayList<CategoryResultModel> namearraylist) {
        this.mcontext = context;
        this.namearraylists = namearraylist;
        this.imagelist = imagelist;
    }


    @Override
    public CategoryListAdaptor.RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_grid_layout, null);
        CategoryListAdaptor.RecyclerHolder recyclerHolder = new CategoryListAdaptor.RecyclerHolder(v);
        return recyclerHolder;
    }

    @Override
    public void onBindViewHolder(CategoryListAdaptor.RecyclerHolder holder, int position) {
//        CategoryModel categoryModel1 = namearraylist.get(position);
        CategoryResultModel categoryResultModel = namearraylists.get(position);
        Picasso.with(mcontext).load(IMAGES_URL+ CategoryActivity.object.get(position)).into(holder.imageView);
        if (CategoryActivity.object.get(position).equals("null")){
            holder.imageView.setImageResource(R.drawable.ic_card_view);
        }
        holder.textView.setText(categoryResultModel.getTitle());
        double meter = Double.parseDouble(categoryResultModel.getDistance());
        double val = meter/1000;
        DecimalFormat df2 = new DecimalFormat(".###");
        Log.d("valhhuh",df2.format(val));
        holder.disttxt.setText(df2.format(val)+" km");
//        holder.imageView.setImageResource((Integer) imagelist.get(position));
    }
    @Override
    public int getItemCount() {
        return namearraylists.size();
    }
    public class RecyclerHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView,disttxt;

        public RecyclerHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.cat_detail_img);
            textView = (TextView) itemView.findViewById(R.id.cat_detail_txt);
            disttxt =itemView.findViewById(R.id.dist_count);
        }
    }
}

