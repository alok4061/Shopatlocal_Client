package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shopatlocal.User.shopatlocal_client.Fragments.ImageOneFragment;
import com.shopatlocal.User.shopatlocal_client.Fragments.ImageThreeFragment;
import com.shopatlocal.User.shopatlocal_client.Fragments.ImageTwoFragment;


public class ImageViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage = 3;
    private ImageOneFragment imageOneFragment;
    private ImageTwoFragment imageTwoFragment;
    private ImageThreeFragment imageThreeFragment;


    public ImageViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context = context;
        imageOneFragment = new ImageOneFragment();
        imageTwoFragment = new ImageTwoFragment();
        imageThreeFragment = new ImageThreeFragment();

        imageThreeFragment.setContext(context);

    }

    @Override
    public Fragment getItem(int position) {
       // Fragment f = new Fragment();
        Fragment f=new Fragment();
        switch (position) {
            case 0:
                return imageOneFragment;
            case 1:
                return imageTwoFragment;
            case 2:
                return imageThreeFragment;

        }
        return f;
    }
    @Override
    public int getCount() {
        return totalPage;
    }

}

