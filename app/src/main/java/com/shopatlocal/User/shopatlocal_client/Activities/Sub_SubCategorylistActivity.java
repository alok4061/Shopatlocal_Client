package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.shopatlocal.User.shopatlocal_client.Adaptors.CategoryClickListner;
import com.shopatlocal.User.shopatlocal_client.Adaptors.Sub_SubCategoryAdaptor;
import com.shopatlocal.User.shopatlocal_client.Adaptors.Sub_SubCategoryClickListener;
import com.shopatlocal.User.shopatlocal_client.R;

public class Sub_SubCategorylistActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoties);
        RecyclerView subcat_rec = findViewById(R.id.sub_cat_recyclerview);
        RecyclerView.LayoutManager  layoutManager =new LinearLayoutManager(this);
        subcat_rec.setLayoutManager(layoutManager);
        Sub_SubCategoryAdaptor categoryAdaptor =new Sub_SubCategoryAdaptor();
        subcat_rec.setAdapter(categoryAdaptor);
        subcat_rec.addOnItemTouchListener(new Sub_SubCategoryClickListener(getApplicationContext(), new CategoryClickListner.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(Sub_SubCategorylistActivity.this,profileActivity.class);
                startActivity(intent);
            }
        }));
    }
}
