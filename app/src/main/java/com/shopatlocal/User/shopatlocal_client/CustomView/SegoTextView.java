package com.shopatlocal.User.shopatlocal_client.CustomView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by hp on 8/21/2017.
 */

public class SegoTextView extends TextView {
    public SegoTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public SegoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public SegoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("segoeuil.ttf", context);
        setTypeface(customFont);
    }
}
