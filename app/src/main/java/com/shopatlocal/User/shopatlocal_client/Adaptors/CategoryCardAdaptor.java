package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopatlocal.User.shopatlocal_client.Activities.CategoryActivity;
import com.shopatlocal.User.shopatlocal_client.Models.CategoryResultModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CategoryCardAdaptor extends RecyclerView.Adapter<CategoryCardAdaptor.RecyclerHolder> {


    ArrayList<CategoryResultModel> namearraylists = new ArrayList();
    ArrayList imagelist = new ArrayList();
    Context mcontext;
    private static final String IMAGES_URL = "http://192.168.1.13:1339/images/";

    public CategoryCardAdaptor(Context context, ArrayList<CategoryResultModel> namearraylist) {
        this.mcontext = context;
        this.namearraylists = namearraylist;

    }

    @Override
    public CategoryCardAdaptor.RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_category_card_adaptor, null);
        CategoryCardAdaptor.RecyclerHolder recyclerHolder = new CategoryCardAdaptor.RecyclerHolder(v);
        return recyclerHolder;
    }

    @Override
    public void onBindViewHolder(CategoryCardAdaptor.RecyclerHolder holder, int position) {

        CategoryResultModel categoryResultModel = namearraylists.get(position);
        Picasso.with(mcontext).load(IMAGES_URL+ CategoryActivity.object.get(position)).into(holder.imageView);
        if (CategoryActivity.object.get(position).equals("null")){
            holder.imageView.setImageResource(R.drawable.ic_card_view);
        }
        holder.textView.setText(categoryResultModel.getTitle());
        double meter = Double.parseDouble(categoryResultModel.getDistance());
        double val = meter/1000;
        DecimalFormat df2 = new DecimalFormat(".###");
        Log.d("valhhuh",df2.format(val));
        holder.disttxt.setText(df2.format(val)+" km");
//        holder.imageView.setImageResource((Integer) imagelist.get(position));
    }

    @Override
    public int getItemCount() {
        return namearraylists.size();
    }

    public class RecyclerHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView,disttxt;

        public RecyclerHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.cat_detail_img);
            textView = (TextView) itemView.findViewById(R.id.cat_detail_txt);
            disttxt =itemView.findViewById(R.id.dist_count);

        }
    }
}


