package com.shopatlocal.User.shopatlocal_client.Utilities;



public class NetworkConstant {

  static String BASE_URL = "http://192.168.1.11:1337/";
  static String IMAGE_BASE_URL="http://192.168.1.11:1339/";

    public static final String csrf_token = BASE_URL + "csrfToken";//get
    public static final String signup = BASE_URL + "api/signup";//post
    public static final String login = BASE_URL + "api/login";//put
    public static final String personal_details=BASE_URL+"api/client/detail";//post//put//get
    public static final String category=BASE_URL+"api/option/categories";//get
    public static final String locationapi = BASE_URL + "api/client/detail/location";//Post
    public static final String upload_image = IMAGE_BASE_URL + "file/upload";//Post
    public static final String post_offers =BASE_URL +"api/client/detail/offer";//post
    public static final String delete_offers =BASE_URL +"api/client/detail/offer";//DELETE
    public static final String offers_list = BASE_URL + "api/client/detail/uniOffer";//Get
    public static final String id_proof_save = BASE_URL + "api/client/detail/proof";//Post
    public static final String id_proof_update = BASE_URL + "api/client/detail/proof";//Put
    public static final String quick_Search = BASE_URL+"api/search";//GET
    public static final String complete_data = BASE_URL+"api/collectiveDetail";//GET
  public static final String get_images = IMAGE_BASE_URL+"images/";
}
