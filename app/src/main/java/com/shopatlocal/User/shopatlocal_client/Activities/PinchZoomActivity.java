package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.shopatlocal.User.shopatlocal_client.CustomView.CustomSliderView;
import com.shopatlocal.User.shopatlocal_client.Models.ImageModel;
import com.shopatlocal.User.shopatlocal_client.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class PinchZoomActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbarTV;
    SliderLayout sliderLayout;
    Intent intent;
    int currentPositon;
    ImageView imgBack;
    ArrayList imageModelArrayList = new ArrayList<>();
    ArrayList object;
    private static final String IMAGES_URL = "http://192.168.1.13:1339/images/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinch_zoom);
        initView();
        sliderLayout.stopAutoCycle();
        intent=getIntent();
        if(intent!=null){
            currentPositon=  intent.getIntExtra("posotion",0     );
            //  Toast.makeText(this, "Postion pinch=" + currentPositon, Toast.LENGTH_SHORT).show();
//            ArrayList ok=  (ArrayList) intent.get("imgArray");
            Intent intent = getIntent();
            Bundle args = intent.getBundleExtra("BUNDLE");
            object = (ArrayList<Object>) args.getSerializable("ARRAYLIST");

            imageModelArrayList= (ArrayList<ImageModel>) intent.getSerializableExtra("imgArray");
            for (int i = 0; i < imageModelArrayList.size(); i++) {
                CustomSliderView view = new CustomSliderView(getApplicationContext());
                Picasso.with(this).load(IMAGES_URL+ object.get(i)).into((Target) view.image((Integer) object.get(i)).setScaleType(BaseSliderView.ScaleType.CenterInside));
//                view.image(String.valueOf(imageModelArrayList.get(i))).setScaleType(BaseSliderView.ScaleType.CenterInside);
//                Utilities.showErrorLog("IMAGE LOCATIONS",imageModelArrayList.get(i).getPic());
                sliderLayout.addSlider(view);
            }
            sliderLayout.setCurrentPosition(currentPositon);
        }
    }
    private void initView() {
//        imgBack=(ImageView) findViewById(R.id.img_back);
//        imgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//        toolbar= (Toolbar) findViewById(R.id.toolbarZoom);
//        toolbar.setBackgroundColor(getResources().getColor(R.color.color_main));
//        toolbarTV.setText("Details");
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
        sliderLayout = (SliderLayout) findViewById(R.id.fullscreen_slider);




    }
}