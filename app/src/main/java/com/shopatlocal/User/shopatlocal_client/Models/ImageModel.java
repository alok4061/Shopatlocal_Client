package com.shopatlocal.User.shopatlocal_client.Models;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by hp on 8/22/2017.
 */

public class ImageModel implements Serializable {

    private Bitmap pic;

    public Bitmap getPic() {
        return pic;
    }

    public void setPic(Bitmap pic) {
        this.pic = pic;
    }
}