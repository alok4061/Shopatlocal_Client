package com.shopatlocal.User.shopatlocal_client.Models;

import java.io.Serializable;

/**
 * Created by hp on 8/28/2017.
 */

public class OffersDetailModel implements Serializable {
    private String title;
    private String acurateAmount;
    private String discountAmount;
    private String valid;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAcurateAmount() {
        return acurateAmount;
    }

    public void setAcurateAmount(String acurateAmount) {
        this.acurateAmount = acurateAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
