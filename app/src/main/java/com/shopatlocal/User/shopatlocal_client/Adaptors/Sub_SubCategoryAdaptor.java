package com.shopatlocal.User.shopatlocal_client.Adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopatlocal.User.shopatlocal_client.R;

public class Sub_SubCategoryAdaptor extends  RecyclerView.Adapter<Sub_SubCategoryAdaptor.RecyclerHolder> {

    String str[] = {"Milk", "Water", "Washed cloth/Iron", "Newspaper", "Outing/Dinner", "Phone bill/Rechage"};
    Integer images[] = {R.drawable.demoimage, R.drawable.ic_launcher_background, R.drawable.ic_launcher_background, R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,R.drawable.ic_launcher_background};

    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_category_adaptor, null);
        RecyclerHolder recyclerHolder = new RecyclerHolder(v);
        return recyclerHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerHolder holder, int position) {
        holder.imageView.setImageResource(images[position]);
        holder.textView.setText(str[position]);


    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class RecyclerHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public RecyclerHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.sub_cat_detail_img);
            textView = (TextView) itemView.findViewById(R.id.sub_cat_detail_txt);
        }
    }
}
