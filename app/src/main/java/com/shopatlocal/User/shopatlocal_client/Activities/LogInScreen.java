package com.shopatlocal.User.shopatlocal_client.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.shopatlocal.User.shopatlocal_client.R;
import com.shopatlocal.User.shopatlocal_client.Utilities.Constants;
import com.shopatlocal.User.shopatlocal_client.Utilities.NetworkConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 7/27/2017.
 */

public class LogInScreen extends AppCompatActivity implements View.OnClickListener {
    Button login;
    TextView signup;
     private EditText contxt,passtxt;
     int mStatusCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        init();
    }

    private void init(){
        login = findViewById(R.id.client_login_btn12);
        signup = findViewById(R.id.signuptxt);
        contxt=findViewById(R.id.logcont);
        passtxt =findViewById(R.id.logpass);
        login.setOnClickListener(this);
        signup.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.client_login_btn12:
                loginapi(contxt.getText().toString(),passtxt.getText().toString());
                break;
            case R.id.signuptxt:
                Intent sign = new Intent(this,RegistrationActivity.class);
                startActivity(sign);
                break;
        }
    }
    private void loginapi(final String mobi, final String pass){

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstant.login, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("res123",s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                    String status = jsonObject.getString("status");
                    Toast.makeText(LogInScreen.this,"Mobile no and Password are not match"+status,Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (mStatusCode==200){
                        String iddata = jsonObject.getString("id");
                        String pass =jsonObject.getString("encryptedPassword");
                        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
                        SharedPreferences.Editor editor =sharedPreferences.edit();
                        editor.putString("User_id",iddata);
                        editor.apply();
                        Intent intent = new Intent(LogInScreen.this,CategoryActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String responseBody = null;
                try {
                    responseBody = new String( volleyError.networkResponse.data, "utf-8" );
                    JSONObject jsonObject = new JSONObject( responseBody );
                    if (jsonObject.getString("status").equals("Unsuccess")){
                        Toast.makeText(LogInScreen.this,"Mobile number and Password are not match",Toast.LENGTH_SHORT).show();
                    }
                    Log.e("res123.......",jsonObject.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }
        }){
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......",mStatusCode+"");
                if (mStatusCode==404){
                    Toast.makeText(LogInScreen.this,"Mobile no and Password are not match",Toast.LENGTH_LONG).show();
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("phone", mobi);
                params.put("password", pass);
                Log.d("params",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
